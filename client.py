#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""
import sys
import socket

# Constantes. Dirección IP del servidor y contenido a enviar

try:
    IP = sys.argv[1]
    PORT = int(sys.argv[2])
    REQUEST = sys.argv[3]
    if REQUEST == 'register':
        REQUEST = REQUEST.upper()
    else:
        sys.exit("Usage: python3 client.py ip port 'register' address")
    ADDRESS = sys.argv[4]
    EXPIRES = int(sys.argv[5])
except IndexError:
    sys.exit("Usage: python3 client.py ip port register address")


# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.connect((IP, PORT))
    SIP = (str(REQUEST) + ' ' 'sip:' + str(ADDRESS) +
           ' ' + 'SIP/2.0\r\n' + 'Expires: ' + str(EXPIRES) + '\r\n\r\n')
    print("Enviando:", SIP)
    my_socket.send(bytes(SIP, 'utf-8'))
    data = my_socket.recv(1024)
    print('Recibido -- ', data.decode('utf-8'))

print("Socket terminado.")
