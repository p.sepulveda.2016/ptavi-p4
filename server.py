#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""
import sys
import socketserver
import json
import time


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    dic = {}

    def json2registered(self):
        """ Descargo contenido del fichero json en el diccionario.
        """
        try:
            with open('registered.json', 'r') as jsonfile:
                self.dic = json.load(jsonfile)
                self.expired()
        except(NameError, FileNotFoundError):
            pass

    def register2json(self):
        """
        Escribir diccionario en formato json.
        """
        with open('registered.json', 'w') as jsonfile:
            json.dump(self.dic, jsonfile, indent=4)

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
        line = self.rfile.read()
        new_line = line.decode('utf-8').split()
        print("Client sends: " + str(self.client_address))
        if new_line[0] == 'REGISTER':
            ip = self.client_address[0]
            address = new_line[1].split(':')    # Separa email del sip
            user = address[1]
            expires_value = new_line[4]
            """
            Tiempo actual + expires_value = time_expires
            """
            time_actual = time.time()
            time_expires = time_actual + int(expires_value)
            time_expires_str = time.strftime('%Y-%m-%d %H:%M:%S',
                                             time.gmtime(time_expires))
            time_actual_str = time.strftime('%Y-%m-%d %H:%M:%S',
                                            time.gmtime(time_actual))
            """ Paso los tiempos a string. """
            self.dic[user] = {'Address: ': ip, 'Expires: ': time_expires_str}
            self.deleted_users = []
            for user in self.dic:
                if time_actual_str >= self.dic[user]['Expires: ']:
                    print("Deleted user, expired.")
                    self.deleted_users.append(user)
                    """
                    Hago una lista con los eliminados
                    y los borro del diccionario.
                    """
        for user in self.deleted_users:
            del self.dic[user]
        print(self.dic)
        self.register2json()


if __name__ == "__main__":
    port = int(sys.argv[1])
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request
    serv = socketserver.UDPServer(('', port), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
